﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CFC.SearchFund
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
                        
            routes.MapRoute("Details", "{controller}/{action}/{kind}/{id}", new { controller = "Search", action = "Details" });
            routes.MapRoute("Default", "{controller}/{action}", new { controller = "Search", action = "Index" });
        }
    }
}
