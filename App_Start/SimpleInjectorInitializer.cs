[assembly: WebActivator.PostApplicationStartMethod(typeof(CFC.SearchFund.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace CFC.SearchFund.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using CFC.SearchFund.Models;
    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
//#error Register your services here (remove this line).

            // For instance:
            container.Register<BaseRepository<CFCSearch_Result>, SearchRepo>(Lifestyle.Scoped);
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
        }
    }
}