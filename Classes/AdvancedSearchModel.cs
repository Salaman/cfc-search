﻿using CFC.SearchFund.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.Classes
{
    public class AdvancedSearchModel
    {
        [Required()]
        [Display(Name = "Строка запроса")]
        [MinLength(2, ErrorMessage = "Необходимо ввести минимум 2 символа") ]
        public string SearchString { get; set; }

        //[Display(Name = "Название субъекта РФ")]
        //public string RussianFederationSubjectName { get; set; }

        //[Display(Name = "Название архивного учереждения")]
        //public string ArchiveInstitution { get; set; }

        //[Display(Name = "Полное название архива")]
        //public string ArchiveFullName { get; set; }

        //[Display(Name = "Результаты запроса")]
        //public List<CFCSearch_Result> Results { get; set; }
    }
}