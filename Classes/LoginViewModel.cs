﻿namespace CFC.SearchFund.Controllers
{
    public class LoginViewModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}