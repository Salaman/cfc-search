﻿using CFC.SearchFund.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.UoW
{
    public class SearchUnit
    {
        public CFCEntities db = new CFCEntities();
        const string CACHE_KEY_SUBJECTS = "{75BD0C5D-0F28-40AF-9CF2-137838D9C1D5}";
        const string CACHE_KEY_FUND_ITEMS_COUNT = "{B2D94B6B-7D32-4413-BB32-219603656763}";
        SearchRepo searchRepository;

        public SearchUnit(SearchRepo repository)
        {
            searchRepository = repository;
        }

        public IEnumerable<SubjectAttributes> GetSubjects()
        {
            
            var existArhivesInSubs = (from tblArh in db.tblARCHIVE
                                      join tblDep in db.tblAuthorizedDep on tblArh.ISN_AuthorizedDep equals tblDep.ISN_AuthorizedDep
                                      where tblDep.KLADR_ID != null && tblArh.NAME != null
                                      select new SubjectGuids
                                      {
                                          Id = tblDep.KLADR_ID
                                      }).Distinct().ToList();

            List<Guid?> extGuids = new List<Guid?>();
            foreach (SubjectGuids su in existArhivesInSubs)
            {
                extGuids.Add(su.Id);
            }

            var subs = (from kladr in db.tblKLADR
                        join kladrSokr in db.tblKLADR_SOKR on kladr.SOKR_ID equals kladrSokr.SOKR_ID
                        where kladr.PARENT_CODE == null && extGuids.Contains(kladr.ID) == true
                        where kladr.PARENT_CODE == null
                        select new SubjectAttributes
                        {
                            FullName = kladr.TERR_NAME + " " + kladrSokr.SOCR_NAME,
                            Id = kladr.ID
                        }).OrderBy(z => z.FullName).ToList();
            
            return subs;
            //Func<IEnumerable<SubjectAttributes>> subs = () => ((CFCEntities)searchRepository.Context).tblKLADR
            //      .Where(x => x.PARENT_CODE == null)
            //      .Join(((CFCEntities)searchRepository.Context).tblKLADR_SOKR,
            //      y => y.SOKR_ID, 
            //      x => x.SOKR_ID, 
            //      (y, x) => new SubjectAttributes {
            //          full_name = y.TERR_NAME + " " + x.SOCR_NAME,
            //          code = y.CODE
            //      })

            //      .Distinct()
            //      .OrderBy(z=> z.full_name)
            //      .ToList();

            //var result = subs.GetCached(CACHE_KEY_SUBJECTS, TimeSpan.FromHours(5));

            //return result;

        }

        public IEnumerable<SubjectAttributes> GetArhives (Guid? Subject)
        {
            var listArhives = (from tblArh in db.tblARCHIVE
                               join tblDep in db.tblAuthorizedDep on tblArh.ISN_AuthorizedDep equals tblDep.ISN_AuthorizedDep
                               where tblDep.KLADR_ID == Subject
                               select new SubjectAttributes
                               {
                                   FullName = tblArh.NAME,
                                   Id = tblArh.ID
                                   
                               }).Distinct().OrderBy(x=>x.FullName).ToList();

            return listArhives;
        }
        //Изменено количество параметров функции SearchPreliminary 10.02.2021 Salaman Andrey
        public int SearchPreliminary(string fulltext_pattern, SearchRepo.SearchType type,
                                                                string subject_name,
                                                                string archive_institution_name,
                                                                string archive_full_name,
                                                                string fund_num_1,
                                                                string fund_num_2,
                                                                string fund_num_3,
                                                                bool flag_fund_name,
                                                                bool flag_inventory_name,
                                                                bool flag_annotation_fund_name,
                                                                bool flag_annotation_inventory_name,
                                                                bool flag_history_name,
                                                                bool flag_unit_name)
        {
            //Использование функции LoadPreliminary с дополнительными параметрами 10.02.2021 Salaman Andrey
            return searchRepository.LoadPreliminary(fulltext_pattern, type,
                                                                subject_name,
                                                                archive_institution_name,
                                                                archive_full_name,
                                                                fund_num_1,
                                                                fund_num_2,
                                                                fund_num_3,
                                                                flag_fund_name,
                                                                flag_inventory_name,
                                                                flag_annotation_fund_name,
                                                                flag_annotation_inventory_name,
                                                                flag_history_name,
                                                                flag_unit_name);
        }

        //private (int Count, SearchRepo.SearchType type, string Error) SearchPrelimeryAsync(string searchString, SearchRepo.SearchType type, out int result, out string error)
        //{
        //    int result = -1;
        //    string error = null;
        //    try
        //    {
        //        result = LogicUnit.SearchPreliminary(searchString.ToFullTextPattern(), type);

        //    }
        //    catch (Exception e)
        //    {
        //        error = e.Message;
        //    }

        //    return new { Count = result, Type = type.ToString(), Error = error });
        //}
        //Изменено количество параметров функции SearchDetalized 10.02.2021 Salaman Andrey
        public ObjectResult<CFCSearch_Result> SearchDetalized(string fulltext_pattern, string match_pattern, string filter, int top_qty, int end_qty,
                                                              string subject_name,
                                                              string archive_institution_name,
                                                              string archive_full_name,
                                                              string fund_num_1,
                                                              string fund_num_2,
                                                              string fund_num_3,
                                                              int flag_fund_name,
                                                              int flag_inventory_name,
                                                              int flag_annotation_fund_name,
                                                              int flag_annotation_inventory_name,
                                                              int flag_history_name,
                                                              int flag_unit_name)
        {
            //Использование функции Load с дополнительными параметрами 10.02.2021 Salaman Andrey
            return searchRepository.Load(fulltext_pattern, match_pattern, filter, top_qty, end_qty,
                                         subject_name,
                                         archive_institution_name,
                                         archive_full_name,
                                         fund_num_1,
                                         fund_num_2,
                                         fund_num_3,
                                         flag_fund_name,
                                         flag_inventory_name,
                                         flag_annotation_fund_name,
                                         flag_annotation_inventory_name,
                                         flag_history_name,
                                         flag_unit_name);
        }


        public int GetFundRecordsCount()
        {
            Func<int> cnt = () => (int)((CFCEntities)searchRepository.Context).vCFCSearch_Fund.Count(x => x.Deleted == 0);
            return cnt.GetCachedValue(CACHE_KEY_FUND_ITEMS_COUNT, TimeSpan.FromHours(10)); ;
        }

    }
}
