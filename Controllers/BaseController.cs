﻿namespace CFC.SearchFund.Controllers
{
    using CFC.SearchFund.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public abstract class BaseController<TEntity> : Controller
        where TEntity : class
    {
        private BaseRepository<TEntity> _repository;

        public BaseRepository<TEntity> Repository
        {
            get
            {
                return _repository;
            }
        }

        //protected BaseController()
        //{
        //    //throw new NotImplementedException();
        //}

        public BaseController(BaseRepository<TEntity> repository)
        {
            _repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _repository != null)
                _repository.Dispose();

            base.Dispose(disposing);
        }

    }
}