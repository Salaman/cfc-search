﻿using CFC.SearchFund.Models;
using CFC.SearchFund.UoW;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using MailKit.Net.Smtp;

namespace CFC.SearchFund.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class SearchController : BaseController<CFCSearch_Result>
    {
        private SearchUnit _LogicUnit;

        public SearchUnit LogicUnit
        {
            get
            {
                if (_LogicUnit == null)
                    _LogicUnit = new SearchUnit((SearchRepo)Repository);

                return _LogicUnit;
            }
        }

        public SearchController(BaseRepository<CFCSearch_Result> repository)
            : base(repository)
        {

        }


        public ActionResult Index()
        {
            var subject_cl = LogicUnit.GetSubjects();
            //return Json(new { Text = "succes Index" }, JsonRequestBehavior.AllowGet);
            //ViewBag.SUBJECT_CL = new SelectList(new string[] { "Все", "Федеральные архивы" }.Union(subject_cl), "Все");
            ViewBag.SUBJECT_CL = new SelectList(subject_cl, "Id", "FullName");
            return View();
        }

        [HttpPost]
        public ActionResult Index(AdvancedModel model)
        {
            if (ViewBag.SUBJECT_CL == null)
            {
                var subject_cl = LogicUnit.GetSubjects();
                //return Json(new { Text = "succes Index(AdvancedModel model)" }, JsonRequestBehavior.AllowGet);
                ViewBag.SUBJECT_CL = new SelectList(subject_cl, "Id", "FullName");
            }
            
            if (model.DetailsViewKey == null)
                return View(model);

            if (model.PrewSearch != model.SearchString && model.PrewSearch != null)
            {
                model.DetailsViewKey = null;
                return View("Index", model);

            }

            //С использованием новой функции в БД - sp_CFCSearch2 11.02.2021 Salaman Andrey
            return IndexDetalized(model);
        }



        [HttpPost]
        public ActionResult IndexDetalized(AdvancedModel model)
        {

            if (ViewBag.SUBJECT_CL == null)
            {
                var subject_cl = LogicUnit.GetSubjects();
                //return Json(new { Text = "succes IndexDetalized(AdvancedModel model)" },JsonRequestBehavior.AllowGet);
                //ViewBag.SUBJECT_CL = new SelectList(new string[] { "Все", "Федеральные архивы" }.Union(subject_cl), "Все");
                ViewBag.SUBJECT_CL = new SelectList(subject_cl, "Id", "FullName");
            }

            if (model == null)
                return View();

            if (string.IsNullOrWhiteSpace(model.SearchString))
            {
                ModelState.Remove("Pattern");
            }

            var pageFrom = 0;
            var pageTo = model.PageCol;

            if (model.PageNumb == null)
            {
                pageFrom = 0;
                pageTo = model.PageCol;
            }
            else
            {
                var pageNumb = Convert.ToInt32(model.PageNumb);
                pageFrom = (pageNumb - 1) * model.PageCol + 1;
                pageTo = pageFrom + model.PageCol;
            }

            List<CFCSearch_Result> results;
            try
            {
                model.withNoErrors = true;
                //Использование новой функции SearchDetalized с дополнительными параметрами 10.02.2021 Salaman Andrey
                results = new List<CFCSearch_Result>(LogicUnit.SearchDetalized(model.SearchString.ToFullTextPattern(), model.SearchString.ToMatchPattern(), model.DetailsViewKey, pageFrom, pageTo,
                                                     model.Subject,
                                                     model.ArchiveInstitution,
                                                     model.ArchiveName,
                                                     model.FundPrefix,
                                                     model.FundNumb,
                                                     model.FundLetter,
                                                     Convert.ToInt32(model.InFundName),
                                                     Convert.ToInt32(model.InInventoryName),
                                                     Convert.ToInt32(model.InFundAnnotate),
                                                     Convert.ToInt32(model.InInventoryAnnotate),
                                                     Convert.ToInt32(model.InFundHistory),
                                                     Convert.ToInt32(model.InUnitName)));

            }

            catch (Exception e)
            {

                model.withNoErrors = false;
                results = new List<CFCSearch_Result>();
            }

            switch (model.Subject)
            {
                case null: break;
                case "Все": break;
                case "Федеральные архивы":
                    results = results.Where(x => x.ArchiveLevel == "a").ToList(); break;
                default:
                    results = results.Where(x => x.SubjectName.Trim().ToUpper() == model.Subject.Trim().ToUpper()).ToList(); break;
            }

            switch (model.ArchiveName)
            {
                case null: break;
                default:
                    results = results.Where(x => x.ArchiveName.Trim().ToUpper() == model.ArchiveName.Trim().ToUpper() || x.ArchiveNameShort.Trim().ToUpper() == model.ArchiveName.Trim().ToUpper()).ToList(); break;
            }
            switch (model.ArchiveInstitution)
            {
                case null: break;
                case "Все": break;
                default:
                    results = results.Where(x => x.ArchiveName.Trim().ToUpper() == model.ArchiveInstitution.Trim().ToUpper() || x.ArchiveNameShort.Trim().ToUpper() == model.ArchiveInstitution.Trim().ToUpper()).ToList(); break;
            }
            //if (!string.IsNullOrWhiteSpace(model.YearFrom))
            //{
            //    results = results.Where(x => x.YearFrom.ToInt(0) >= model.YearFrom.ToInt(0) ||
            //        model.YearFrom == "" && x.YearTo != "").ToList();
            //}


            model.Results = results;

            return View(model);
        }

        public ContentResult AjaxGetSub(Guid? Subject)
        {
            var Arhives = LogicUnit.GetArhives(Subject);  /*new List<string>() { "Архив номер один", "Какой-то еще Архив", "Архив для кого-то", "Кому-то нужный Архив", "Чей-то архив", "Никому не нужный Архив" };*/

            SelectList listArhives = new SelectList(Arhives, "Id", "FullName");

            if (listArhives.Count() != 0)
            {
                string result = "";
                foreach (SelectListItem item in listArhives)
                {
                    result += "<option data-id = \" " + item.Value + " \" value=\' " + item.Text + "\' ></ option >";
                }
                return Content(result);
            }
            else
            {
                return Content(listArhives.Count().ToString());
            }



        }

        public ActionResult Advanced()
        {
            var subject_cl = LogicUnit.GetSubjects();
            //return Json(new { Text = "succes Advanced()" },JsonRequestBehavior.AllowGet);
            //ViewBag.SUBJECT_CL = new SelectList(new string[] { "Все", "Федеральные архивы" }.Union(subject_cl), "Все");
            ViewBag.SUBJECT_CL = new SelectList(subject_cl, "Id", "FullName");


            return View();
        }

        public ActionResult SearchResult(IEnumerable<CFCSearch_Result> data)
        {
            ViewBag.Message = "Это частичное представление.";
            return PartialView(data);
        }

        public ActionResult DetailsModal(System.Guid ID, int Kind)
        {
            CFCEntities cfc = new CFCEntities();

            //var kind = int.Parse(Url.RequestContext.RouteData.Values["kind"].ToString());
            //var id = new Guid(Url.RequestContext.RouteData.Values["id"].ToString());

            var kind = int.Parse(Kind.ToString());
            var id = ID;

            InfoDetails model = new InfoDetails();

            if (kind == 703)
            {
                model.unit = cfc.vCFCSearch_Unit.SingleOrDefault(x => x.ID == id);
            }
            if (kind == 702)
            {
                model.inventory = cfc.vCFCSearch_Inventory.SingleOrDefault(x => x.ID == id);
            }
            if (kind == 701)
            {
                model.fund = cfc.vCFCSearch_Fund.SingleOrDefault(m => m.ID == id);
            }
            if (kind == 700)
            {
                model.archive = cfc.tblARCHIVE.SingleOrDefault(x => x.ID == id);
            }
            if (model.unit != null)
            {
                model.inventory = cfc.vCFCSearch_Inventory.SingleOrDefault(x => x.ISN_INVENTORY == model.unit.ISN_INVENTORY);
            }
            if (model.inventory != null)
            {
                model.fund = cfc.vCFCSearch_Fund.SingleOrDefault(x => x.ISN_FUND == model.inventory.ISN_FUND);
            }
            if (model.fund != null)
            {
                model.archive = cfc.tblARCHIVE.SingleOrDefault(x => x.ISN_ARCHIVE == model.fund.ISN_ARCHIVE);
            }
            if (model.archive != null)
            {
                model.subject = cfc.tblSUBJECT_CL.SingleOrDefault(x => x.ISN_SUBJECT == model.archive.ISN_SUBJECT);
            }
            //var data  = ViewBag.Message = "Это частичное представление.";
            return PartialView("Partial/DetailsModal", model);
        }

        public ActionResult SleepResult(string TYPE)
        {
            Thread.Sleep(5000);
            //var model = TYPE;
            return Content(TYPE);
        }

        [HttpPost]
        //Изменены параметры функции GetSearchPrelimaryData 10.02.2021 Salaman Andrey
        public ActionResult GetSearchPrelimaryData(string searchString, SearchRepo.SearchType type,
                                                                string subject_name,
                                                                string archive_institution_name,
                                                                string archive_full_name,
                                                                string fund_num_1,
                                                                string fund_num_2,
                                                                string fund_num_3,
                                                                bool flag_fund_name,
                                                                bool flag_inventory_name,
                                                                bool flag_annotation_fund_name,
                                                                bool flag_annotation_inventory_name,
                                                                bool flag_history_name,
                                                                bool flag_unit_name)
        {
            int result = -1;
            string error = null;
            try
            {
                //Использование функции SearchPreliminary с дополнительными параметрами 10.02.2021 Salaman Andrey
                result = LogicUnit.SearchPreliminary(searchString.ToFullTextPattern(), type,
                                                                subject_name,
                                                                archive_institution_name,
                                                                archive_full_name,
                                                                fund_num_1,
                                                                fund_num_2,
                                                                fund_num_3,
                                                                flag_fund_name,
                                                                flag_inventory_name,
                                                                flag_annotation_fund_name,
                                                                flag_annotation_inventory_name,
                                                                flag_history_name,
                                                                flag_unit_name);
            }
            catch (Exception e)
            {
                do
                {
                    error = e.Message;
                    e = e.InnerException;
                }
                while (e != null);
            }
            //return Json(new { Text = "GetSearchPrelimaryData()" }, JsonRequestBehavior.AllowGet);
            return Json(new { Count = result, Type = type.ToString(), Error = error });
        }

        [HttpPost]
        public ActionResult CheckCertificate2()
        {
            Thread.Sleep(TimeSpan.FromMinutes(1));

            return Json(new { Text = "succes" });
        }

        [HttpPost]
        //Изменение параемтров функции GetSearchPrelimaryData2 10.02.2021 Salaman Andrey
        public async Task<ActionResult> GetSearchPrelimaryData2(string searchString, SearchRepo.SearchType type,
                                                                string subject_name,
                                                                string archive_institution_name,
                                                                string archive_full_name,
                                                                string fund_num_1,
                                                                string fund_num_2,
                                                                string fund_num_3,
                                                                bool flag_fund_name,
                                                                bool flag_inventory_name,
                                                                bool flag_annotation_fund_name,
                                                                bool flag_annotation_inventory_name,
                                                                bool flag_history_name,
                                                                bool flag_unit_name)
        {
            //Использование функции GetSearchPrelimaryData с дополнительными параметрами 10.02.2021 Salaman Andrey
            var task = Task.Run(() => GetSearchPrelimaryData(searchString, type,
                                                                subject_name,
                                                                archive_institution_name,
                                                                archive_full_name,
                                                                fund_num_1,
                                                                fund_num_2,
                                                                fund_num_3,
                                                                flag_fund_name,
                                                                flag_inventory_name,
                                                                flag_annotation_fund_name,
                                                                flag_annotation_inventory_name,
                                                                flag_history_name,
                                                                flag_unit_name));
            var res = await task;
            //return Json(new { Text = "GetSearchPrelimaryData2()" }, JsonRequestBehavior.AllowGet);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        //FeedbackData
        public class FeedbackData
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public string Subject { get; set; }
            public string Message { get; set; }
            public string Phone { get; set; }
        }
        public ContentResult SendMail(FeedbackData FeedbackData)
        {
            string result = "";
            if (FeedbackData.Name != null && FeedbackData.Address != null && FeedbackData.Subject != null && FeedbackData.Message != null)
            {
                if (!FeedbackData.Address.Contains('@') && !FeedbackData.Address.Contains('.'))
                {
                    result = "email_error";
                    return Content(result);
                }
                string targetEmail = "archives-projects@yandex.ru";
                string SMTPName = "CFC-Search";
                string SMTPUser = "cfc-search@mail.ru";
                string SMTPPassword = "a1b2c3d4e5d6";
                int SMTPPort = 465;  //465 - outgoing ssl port
                string SMTPServer = "smtp.mail.ru";

                var mailMessage = new MimeKit.MimeMessage();
                mailMessage.From.Add(new MimeKit.MailboxAddress(SMTPName, SMTPUser));
                mailMessage.To.Add(new MimeKit.MailboxAddress(targetEmail.Substring(0, targetEmail.IndexOf('@')), targetEmail));
                mailMessage.Subject = FeedbackData.Subject;
                string body = FeedbackData.Message + "\n\n--\n" + FeedbackData.Name + "\n" + FeedbackData.Phone + "\n" + FeedbackData.Address;
                mailMessage.Body = new MimeKit.TextPart("plain")
                {
                    Text = body
                };

                var mailMessage2 = new MimeKit.MimeMessage();
                mailMessage2.From.Add(new MimeKit.MailboxAddress(SMTPName, SMTPUser));
                mailMessage2.To.Add(new MimeKit.MailboxAddress(FeedbackData.Address.Substring(0, FeedbackData.Address.IndexOf('@')), FeedbackData.Address));
                mailMessage2.Subject = FeedbackData.Subject;
                mailMessage2.Body = new MimeKit.TextPart("plain")
                {
                    Text = FeedbackData.Name + "! Ваше обращение зарегистрировано."
                };

                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Connect(SMTPServer, SMTPPort, true);
                    smtpClient.Authenticate(SMTPUser, SMTPPassword);
                    smtpClient.Send(mailMessage);
                    smtpClient.Send(mailMessage2);
                    smtpClient.Disconnect(true);
                }
                result = "success";
            }
            return Content(result);

        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600, VaryByParam = "none")]
        public ActionResult FundRecordsCount()
        {
            return Content(LogicUnit.GetFundRecordsCount().ToString());
        }

        public ActionResult About()
        {

            return View();
        }
        public ActionResult Questions()
        {

            return View();
        }
        public ActionResult ListQuestions()
        {

            return View();
        }
        public ActionResult ServiceInfo()
        {

            return View();
        }
    }
}
