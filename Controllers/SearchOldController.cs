﻿using CFC.SearchFund.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CFC.SearchFund.Controllers
{
    public class SearchOldController : Controller
    {
        //
        // GET: /Search/

        public ActionResult Info()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View("Basic");
        }

        public ActionResult Basic()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Basic(BasicModel model)
        {
            if (ModelState.IsValid)
            {
                List<CFCSearch_Result> results;
                model.IsReloading = true;

                using (CFCEntities cfc = new CFCEntities())
                {
                    try
                    {
                        model.withNoErrors = true;
                        model.Pattern = String.Join(" ", model.Pattern.Split(')', '(', '\'', '"', '%', ']', '[', '#', '@', '.'));
                        //results = new List<CFCSearch_Result>(cfc.CFCSearchFromCashFunction(model.Pattern.ToFullTextPattern(), model.Pattern.ToMatchPattern(), "InFundName"));
                        //Заглушка на функцию sp_CFCSearch 10.02.2021 Salaman Andrey
                        results = new List<CFCSearch_Result>(cfc.sp_CFCSearch(model.Pattern.ToFullTextPattern(), model.Pattern.ToMatchPattern(), "InFundName", 0, 100,
                                                             "",
                                                             "",
                                                             "",
                                                             "",
                                                             "",
                                                             "",
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0));
                    }
                    catch (Exception e)
                    {
                        model.withNoErrors = false;
                        if (e.InnerException.Message.Length > 0)
                        {
                            model.MsgErrors = "Исключение " + e.InnerException.Message;
                        }
                        else
                        {
                            model.MsgErrors = e.Message;
                        }
                        results = new List<CFCSearch_Result>();
                    }

                }

                model.Results = results;

                return View("Results", model);

            }

            return View(model);
        }

       
        public ActionResult Results()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Results(BasicModel model)
        {
            return Basic(model);
        }

        public ActionResult Advanced()
        {
            CFCEntities cfc = new CFCEntities();
            var subject_cl = cfc.tblSUBJECT_CL.Select(x => x.NAME).Distinct();
            ViewBag.SUBJECT_CL = new SelectList(new string[] { "Все", "Федеральные архивы" }.Union(subject_cl), "Все");

            return View();
        }

        [HttpPost]
        public ActionResult Advanced(AdvancedModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Pattern))
            {
                ModelState.Remove("Pattern");
            }
            if (ModelState.IsValid)
            {
                var filter = "";
                                if (model.InFundName)
                {
                    filter += "InFundName;";
                }
                if (model.InFundAnnotate)
                {
                    filter += "InFundAnnotate;";
                }
                if (model.InFundHistory)
                {
                    filter += "InFundHistory;";
                }
                if (model.InInventoryName)
                {
                    filter += "InInventoryName;";
                }
                if (model.InInventoryAnnotate)
                {
                    filter += "InInventoryAnnotate;";
                }
                if (model.InUnitName)
                {
                    filter += "InUnitName;";
                }


                CFCEntities cfc = new CFCEntities();
                List<CFCSearch_Result> results;
                try
                {
                    model.withNoErrors = true;
                    //Заглушка на функцию sp_CFCSearch 10.02.2021 Salaman Andrey
                    results = new List<CFCSearch_Result>(cfc.sp_CFCSearch(model.Pattern.ToFullTextPattern(), model.Pattern.ToMatchPattern(), filter,0,100,
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0));

                }
               
                catch  (Exception e)
                {

                    model.withNoErrors = false;
                    results = new List<CFCSearch_Result>();
                }

                switch (model.Subject)
                {
                    case null: break;
                    case "Все": break;
                    case "Федеральные архивы":
                        results = results.Where(x => x.ArchiveLevel == "a").ToList(); break;
                    default:
                        results = results.Where(x => x.SubjectName == model.Subject).ToList(); break;
                }

                switch (model.ArchiveName)
                {
                    case null: break;
                    default:
                        results = results.Where(x => x.ArchiveName == model.ArchiveName).ToList(); break;
                }
                switch (model.ArchiveNameShort)
                {
                    case null: break;
                    default:
                        results = results.Where(x => x.ArchiveNameShort == model.ArchiveNameShort).ToList(); break;
                }
                //if (!string.IsNullOrWhiteSpace(model.YearFrom))
                //{
                //    results = results.Where(x => x.YearFrom.ToInt(0) >= model.YearFrom.ToInt(0) ||
                //        model.YearFrom == "" && x.YearTo != "").ToList();
                //}
                

                model.Results = results;

                return View("Results", model);
            }

            return Advanced();
        }

        public ActionResult Details(int kind, Guid id)
        {
            return View();
        }

        [HttpPost]
        public string TEST()
        {
            return ("TEST");
        }

        [HttpPost]
        public ActionResult ResultsPart(BasicModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    model.IsReloading = true;
            var RequestString = Request.Form.ToString();
            RequestString = RequestString.Substring(2, RequestString.Length - 2);
            var Pattern = "";
            var Filter = "InFundName";
            Pattern = RequestString.Substring(0, RequestString.IndexOf("&b="));
            //RequestString = RequestString.Substring(RequestString.IndexOf("&b=") + 3, RequestString.Length - RequestString.IndexOf("&b=") );
            RequestString = RequestString.Substring(RequestString.IndexOf("&b=") + 3, RequestString.Length - RequestString.IndexOf("&b=")-3);
            
            Filter = RequestString.Replace("%3b", ";");
            Pattern = Pattern.Replace("RR01RR", "й");
            Pattern = Pattern.Replace("RR02RR", "ц");
            Pattern = Pattern.Replace("RR03RR", "у");
            Pattern = Pattern.Replace("RR04RR", "к");
            Pattern = Pattern.Replace("RR05RR", "е");
            Pattern = Pattern.Replace("RR06RR", "н");
            Pattern = Pattern.Replace("RR07RR", "г");
            Pattern = Pattern.Replace("RR08RR", "ш");
            Pattern = Pattern.Replace("RR09RR", "щ");
            Pattern = Pattern.Replace("RR10RR", "з");
            Pattern = Pattern.Replace("RR11RR", "х");
            Pattern = Pattern.Replace("RR12RR", "ъ");
            Pattern = Pattern.Replace("RR13RR", "ф");
            Pattern = Pattern.Replace("RR14RR", "ы");
            Pattern = Pattern.Replace("RR15RR", "в");
            Pattern = Pattern.Replace("RR16RR", "а");
            Pattern = Pattern.Replace("RR17RR", "п");
            Pattern = Pattern.Replace("RR18RR", "р");
            Pattern = Pattern.Replace("RR19RR", "о");
            Pattern = Pattern.Replace("RR20RR", "л");
            Pattern = Pattern.Replace("RR21RR", "д");
            Pattern = Pattern.Replace("RR22RR", "ж");
            Pattern = Pattern.Replace("RR23RR", "э");
            Pattern = Pattern.Replace("RR24RR", "я");
            Pattern = Pattern.Replace("RR25RR", "ч");
            Pattern = Pattern.Replace("RR26RR", "с");
            Pattern = Pattern.Replace("RR27RR", "м");
            Pattern = Pattern.Replace("RR28RR", "и");
            Pattern = Pattern.Replace("RR29RR", "т");
            Pattern = Pattern.Replace("RR30RR", "ь");
            Pattern = Pattern.Replace("RR31RR", "ё");
            Pattern = Pattern.Replace("RR32RR", "б");
            Pattern = Pattern.Replace("RR33RR", "ю");


            model.Pattern = Pattern;// FormatSearchRecoding();
         //  model.Pattern = Request.Form.ToString();


                CFCEntities cfc = new CFCEntities();
                List<CFCSearch_Result> results;
                try
                {
                    model.withNoErrors = true;
                    //Заглушка на функцию sp_CFCSearch 10.02.2021 Salaman Andrey
                    results = new List<CFCSearch_Result>(cfc.sp_CFCSearch(model.Pattern.ToFullTextPattern(), model.Pattern.ToMatchPattern(), Filter, 101, 100000,
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0));
                    //пронумеровать надо во вьюхе
                    //var productsPart = dbContext.Products.OrderBy(p => p.ProductID).Where(p => p.ProductID > (int)lastId).Take(2);
                }
                catch (Exception e)
                {
                    model.withNoErrors = false;
                    if (e.InnerException.Message.Length > 0)
                    {
                        model.MsgErrors = "Исключение " + e.InnerException.Message;
                    }
                    else
                    {
                        model.MsgErrors = e.Message;
                    }
                    results = new List<CFCSearch_Result>();
                }
                model.Results = results;
                return PartialView("ResultElementPart", model);
            //}

            //return View(model);
        }
    }
}