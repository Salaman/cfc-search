﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CFC.SearchFund
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        
        protected void Session_Start()
        {
            try
            {
                using (var cfc = new Models.CFCEntities())
                {
                    var log = new Models.eqActivityLog();

                    log.ID = Guid.NewGuid();
                    log.CreationDateTime = DateTime.Now;
                    log.OwnerID = new Guid("12345678-9012-3456-7890-123456789012");
                    log.StatusID = new Guid("12345678-9012-3456-7890-123456789012");
                    log.Deleted = 0;

                    log.ActivityDate = DateTime.Now;
                    log.Login = "SEARCH_USER";
                    log.Pass = null;
                    log.LogonRezult = true;
                    log.UserAgent = Request.UserAgent;
                    log.UserHostAddress = Request.UserHostAddress;
                    log.Req = Request.ToString();

                    cfc.eqActivityLog.Add(log);
                    cfc.SaveChanges();
                }
            }
            catch { }
        }
    }
}
