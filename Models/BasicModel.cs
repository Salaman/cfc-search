﻿namespace CFC.SearchFund.Models
{
    using CFC.SearchFund.Classes;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class BasicModel : AdvancedSearchModel
    {
        [Required()]
        [Display(Name = "Строка запроса")]
        public string Pattern { get; set; }

        [Display(Name = "Результаты запроса")]
        public IEnumerable<CFCSearch_Result> Results { get; set; }
        //Roman 16.07.13
        //Свойство добавлено с целью передачи в Results.cshtml информации о том, что поисковый запрос вызвал ошибку, и не был обработан.
        //Делая это, я преследовал сомнительную цель вывода информации об обработанном исключении, пусть и таким странным путем.
        [Display(Description = "Запрос отработал без ошибок", Name = "Запрос отработал без ошибок")]
        public bool withNoErrors { get; set; }
        [Display(Name = "Описание ошибки")]
        public string MsgErrors { get; set; }
        //Natalia 18.12.13
        [Display(Name = "Дозагрузка")]
        public bool IsReloading { get; set; }
        
        
    }

    public class AdvancedModel : BasicModel
    {

        [Display(Name = "Название субъекта Российской Федерации")]
        public string Subject { get; set; }

        [Display(Name = "Название архивного учереждения")]
        public string ArchiveInstitution { get; set; }

        [Display(Name = "Сокращенное название архива")]
        public string ArchiveNameShort { get; set; }

        [Display(Name = "Полное название архива")]
        public string ArchiveName { get; set; }

        [Display(Name = "Название фонда")]
        public bool InFundName { get; set; }

        [Display(Name = "Аннотация фонда", Description = "Если информация введена в систему")]
        public bool InFundAnnotate { get; set; }

        [Display(Name = "Историческая справка фонда", Description = "Если информация введена в систему")]
        public bool InFundHistory { get; set; }

        [Display(Name = "Название описи", Description = "Если информация введена в систему")]
        public bool InInventoryName { get; set; }

        [Display(Name = "Аннотация описи", Description = "Если информация введена в систему")]
        public bool InInventoryAnnotate { get; set; }

        [Display(Name = "Название единицы хранения", Description = "Если информация введена в систему")]
        public bool InUnitName { get; set; }

        //по номеру фонда
        //Изменен тип на string для простоты 10.02.2021 Salaman Andrey
        [Display(Name = "Префикс фонда")]
        public string FundPrefix { get; set; }
        //Изменен тип на string для простоты 10.02.2021 Salaman Andrey
        [Display(Name = "Номер фонда")]
        public string FundNumb { get; set; }
        //Изменен тип на string для простоты 10.02.2021 Salaman Andrey
        [Display(Name = "Литера фонда")]
        public string FundLetter { get; set; }

        public string DetailsViewKey { get; set; }
        public string PageNumb { get; set; } = "1";
        public string PrewPageNumb { get; set; } = "1";
        public int PageCol { get; set; } = 10;
        public bool NextElem { get; set; }

        public string PrewSearch { get; set; }

        public int AllResultsOfType { get; set; }
    }

    public class SubjectAttributes
    {
        [Display(Name = "Код")]
        public Guid? Id { get; set; }

        [Display(Name = "Полное имя")]
        public string FullName { get; set; }
        
        
    }

    public class SubjectGuids
    {
        public Guid? Id { get; set; }
    }

}