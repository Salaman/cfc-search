﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.Models
{
    public static class EFExtensions
    {

        public static string GetKeyNames(this System.Data.Entity.Core.Objects.ObjectContext context, Type entityType)
        {
            var metadata = context.MetadataWorkspace;

            var objectItemCollection = ((ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace));

            // Get metadata for given CLR type
            var entityMetadata = metadata
                    .GetItems<EntityType>(DataSpace.OSpace)
                    .Single(e => objectItemCollection.GetClrType(e) == entityType);

            return entityMetadata.KeyProperties.Select(p => p.Name).FirstOrDefault();
        }

        private static IEnumerable<Type> GetAncestors(Type type)

        {

            Type current = type;

            do

            {

                yield return current;

                current = current.BaseType;

            } while (current != typeof(object));



        }

        public static string GetSingleEntitySetName<TEntity>(this ObjectContext context)
            where TEntity : class
        {
            return context.GetSingleEntitySetName(typeof(TEntity));
        }
        
        public static string GetSingleEntitySetName(this ObjectContext context, Type entityType)

        {
            var ancestors = GetAncestors(entityType).ToList();
            var workspace = context.MetadataWorkspace;
            var container = workspace.GetEntityContainer(context.DefaultContainerName, DataSpace.CSpace);
            var objectItems = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            
            var entitySetNames =
                from set in container.BaseEntitySets.OfType<EntitySet>()
                let conceptualElementType = set.ElementType
                let objectElementType = workspace.GetObjectSpaceType(conceptualElementType)
                let clrElementType = objectItems.GetClrType(objectElementType)
                where ancestors.Any(type => type == clrElementType)
                select set.Name;

            return $"{container.Name}.{entitySetNames.Single()}";

        }
        
        public static T GetCached<T>(this Func<T> func, string cacheKey, TimeSpan expiration)
            where T: class
        {
            var cacheResult = HttpRuntime.Cache[cacheKey] as T;

            if (cacheResult == null)
            {
                cacheResult = func();
                HttpRuntime.Cache.Insert(cacheKey, cacheResult, null, DateTime.Now + expiration, System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return cacheResult;
        }
        
        public static T GetCachedValue<T>(this Func<T> func, string cacheKey, TimeSpan expiration)
            where T: struct
        {
            var cacheResult = HttpRuntime.Cache.Get(cacheKey);

            if (cacheResult == null)
            {
                cacheResult = func();
                HttpRuntime.Cache.Insert(cacheKey, cacheResult, null, DateTime.Now + expiration, System.Web.Caching.Cache.NoSlidingExpiration);                
            }

            return (T)cacheResult;
        }
    }
}