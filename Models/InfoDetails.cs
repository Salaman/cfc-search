﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.Models
{
    public class InfoDetails
    {
        public tblSUBJECT_CL subject { get; set; }
        public tblARCHIVE archive { get; set; }
        public vCFCSearch_Fund fund { get; set; }
        public vCFCSearch_Inventory inventory { get; set; }
        public vCFCSearch_Unit unit { get; set; }
    }
}