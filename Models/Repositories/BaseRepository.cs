﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.Models
{
    public abstract class BaseRepository<TEntity> : IDisposable
        where TEntity : class
    {
        private string _entityTypeFullName;

        static volatile ConcurrentDictionary<string, string> _edmSetNameStore = new ConcurrentDictionary<string, string>();

        static volatile ConcurrentDictionary<string, string> _edmKeyStore = null;

        private static readonly object lockObj = new object();

        private static ConcurrentDictionary<string, string> EdmKeyStore
        {
            get
            {
                /// Синглтон с двойной блокировкой
                if (_edmKeyStore != null)
                    return _edmKeyStore;

                lock (lockObj)
                {
                    if (_edmKeyStore == null)
                    {

                        var tmpStore = new ConcurrentDictionary<string, string>();
                        _edmKeyStore = tmpStore;
                    }
                }

                return _edmKeyStore;
            }
        }

        private System.Data.Entity.Core.Objects.ObjectContext _ObjectContext
        {
            get
            {
                if (Context == null)
                    throw new ArgumentNullException(nameof(Context), "Context not initialized");

                return ((IObjectContextAdapter)Context).ObjectContext;
            }
        }

        public abstract DbContext Context { get; }

        private DbSet<TEntity> _DbSet;

        protected DbSet<TEntity> DbSet
        {
            get
            {
                if (_DbSet == null)
                    _DbSet = Context.Set<TEntity>();

                return _DbSet;
            }
        }

        public BaseRepository()
        {
            //var entityType = typeof(TEntity);
            //_entityTypeFullName = entityType.FullName;

            //// Detect entity key names
            //if (!EdmKeyStore.ContainsKey(_entityTypeFullName))
            //{
            //    var keyName = _ObjectContext.GetKeyNames(entityType);
            //    EdmKeyStore[_entityTypeFullName] = keyName;
            //}

            //// Detect entity DbSet names
            //if (!_edmSetNameStore.ContainsKey(_entityTypeFullName))
            //{
            //    var entitySetName = _ObjectContext.GetSingleEntitySetName<TEntity>();
            //    _edmSetNameStore[_entityTypeFullName] = entitySetName;
            //}
        }

        public virtual System.Data.Entity.Core.Objects.ObjectSet<TEntity> Load()
        {
            return _ObjectContext.CreateObjectSet<TEntity>();
        }
        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual void Delete(Guid id, bool forceDbRemove = true)
        {
            var entity = DbSet.Find(id);

            Delete(entity);
        }

        public virtual void Delete(TEntity entity, bool forceDbRemove = true)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            DbSet.Remove(entity);
        }

        public virtual void Save(TEntity entity)
        {
            Context.SaveChanges();
        }

        public virtual TEntity GetById(Guid Id)
        {
            //var entityKeyValues = new KeyValuePair<string, object>[] {
            //    new KeyValuePair<string, object>(EdmKeyStore[_entityTypeFullName], Id) };

            //EntityKey key = new EntityKey(_edmSetNameStore[_entityTypeFullName], entityKeyValues);


            //if (!_ObjectContext.TryGetObjectByKey(key, out var entity))
            //{
            //    throw new ObjectNotFoundException($"В БД отсутствует объект типа [{typeof(TEntity).Name}] с ключем ID=['{typeof(TEntity).Name}']");
            //}

            var entity = DbSet.Find(Id);


            return entity;
        }

        protected EntityKey CreateEntityKey(Guid Id)
        {
            var entityKeyValues = new KeyValuePair<string, object>[] {
                new KeyValuePair<string, object>(EdmKeyStore[_entityTypeFullName], Id) };

            return new EntityKey(_edmSetNameStore[_entityTypeFullName], entityKeyValues);
        }


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}