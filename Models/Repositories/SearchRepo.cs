﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace CFC.SearchFund.Models
{
    public class SearchRepo : BaseRepository<CFCSearch_Result>
    {
        public enum SearchType
        {
            InFundName = 1,

            InFundHistory = 2,

            InFundAnnotate = 3,

            InInventoryName = 4,

            InInventoryAnnotate = 5,

            InUnitName = 6,

            Undefined = 0
        }

        public override DbContext Context => new CFCEntities();

        //public override CFCSearch_Result GetById(Guid Id)
        //{
        //    throw new NotImplementedException();
        //}
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ObjectSet<CFCSearch_Result> Load()
        {
            throw new NotImplementedException();
        }
        //Изменено количество параметров функции Load 10.02.2021 Salaman Andrey
        public ObjectResult<CFCSearch_Result> Load(string fulltext_pattern, string match_pattern, string filter, int top_qty, int end_qty,
                                                   string subject_name,
                                                   string archive_institution_name,
                                                   string archive_full_name,
                                                   string fund_num_1,
                                                   string fund_num_2,
                                                   string fund_num_3,
                                                   int flag_fund_name,
                                                   int flag_inventory_name,
                                                   int flag_annotation_fund_name,
                                                   int flag_annotation_inventory_name,
                                                   int flag_history_name,
                                                   int flag_unit_name)
        {
            //Использование функции sp_CFCSearch с дополнительными параметрами 10.02.2021 Salaman Andrey
            return ((CFCEntities)Context).sp_CFCSearch(fulltext_pattern, match_pattern, filter, top_qty, end_qty,
                                                       subject_name,
                                                       archive_institution_name,
                                                       archive_full_name,
                                                       fund_num_1,
                                                       fund_num_2,
                                                       fund_num_3,
                                                       flag_fund_name,
                                                       flag_inventory_name,
                                                       flag_annotation_fund_name,
                                                       flag_annotation_inventory_name,
                                                       flag_history_name,
                                                       flag_unit_name);
        }
        //Добавлены параметры в функцию LoadPreliminary 10.02.2021 Salaman Andrey
        public int LoadPreliminary(string fulltext_pattern, SearchType type,
                                                                string subject_name,
                                                                string archive_institution_name,
                                                                string archive_full_name,
                                                                string fund_num_1,
                                                                string fund_num_2,
                                                                string fund_num_3,
                                                                bool flag_fund_name,
                                                                bool flag_inventory_name,
                                                                bool flag_annotation_fund_name,
                                                                bool flag_annotation_inventory_name,
                                                                bool flag_history_name,
                                                                bool flag_unit_name)
        {
            //Использование функции sp_CFCSearchPreliminary с дополнительными параметрами 10.02.2021 Salaman Andrey
            var res = ((CFCEntities)Context).sp_CFCSearchPreliminary(fulltext_pattern, (int)type,
                                                                        subject_name,
                                                                        archive_institution_name,
                                                                        archive_full_name,
                                                                        fund_num_1,
                                                                        fund_num_2,
                                                                        fund_num_3,
                                                                        Convert.ToInt32(flag_fund_name),
                                                                        Convert.ToInt32(flag_inventory_name),
                                                                        Convert.ToInt32(flag_annotation_fund_name),
                                                                        Convert.ToInt32(flag_annotation_inventory_name),
                                                                        Convert.ToInt32(flag_history_name),
                                                                        Convert.ToInt32(flag_unit_name));
            return res.First().Count.GetValueOrDefault();
        }
    }
}