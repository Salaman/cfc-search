﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CFC.SearchFund.Models
{
    public static class SearchExtensions
    {
        public static string ToFullTextPattern(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "*";
            }

            string result = "";

            if (value.Contains("\"")) //Обработка кавычек (к примеру "Романов") 16.02.2021
                value = value.Replace("\"", "");
            
            foreach (var word in value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (result != "")
                {
                    result += " AND ";
                }

                result += string.Format(" (FORMSOF (INFLECTIONAL, {0}) OR \"{0}\") ", word);
            }

            return result;
        }

        public static string ToMatchPattern(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "*";
            }
            string result = "%";
            foreach (var word in value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
            {
                result += word + "%";
            }
            return result;
        }


        public static string TEST(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "*";
            }
            string result = "%";
            foreach (var word in value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
            {
                result += word + "%";
            }
            return result;
        }
        
        public static MvcHtmlString FormatSearch(this HtmlHelper helper, string Search, string Pattern)
        {
            string result = Search;
            var p = StringComparison.InvariantCultureIgnoreCase;

            if (result.Length > 200)
            {
                int n0 = result.IndexOf(Pattern, p);
                int n1 = Math.Max(n0 - 100, 0);
                int n2 = Math.Min(n0 + Pattern.Length + 100, result.Length);

                result = "..." + result.Substring(n1, n2 - n1) + "...";
            }

            int r1 = result.IndexOf(Pattern, p);
            int r2 = r1 + Pattern.Length;

            result = HttpUtility.HtmlEncode(result.Substring(0, r1)) + "<b>" + Pattern + "</b>" + HttpUtility.HtmlEncode(result.Substring(r2));
            return MvcHtmlString.Create(result);
        }


        public static MvcHtmlString FormatSearchCoding(this HtmlHelper<BasicModel> helper,  string Pattern)
        {
            string result="";
            if (string.IsNullOrWhiteSpace(Pattern))
            {
                return MvcHtmlString.Create("*");
            }

            string intermediate_str = Pattern.ToLower();
            char intermediate ;

            foreach (var symbol in intermediate_str)
            {

                intermediate = symbol;
                var i = CyrToCode(symbol);
                if (i !="0")
                {
                    result = result + "RR" + i + "RR";
                }
                else
                {
                    result = result + symbol;
                }
                
            }
            
            return MvcHtmlString.Create(result);
        }


        public static string FormatSearchRecoding(string Pattern)
        {
            string result = "";
            if (string.IsNullOrWhiteSpace(Pattern))
            {
                return " ";
            }
            result = Pattern.Replace ("RR01RR","й");
            result = Pattern.Replace("RR02RR", "ц");
            result = Pattern.Replace("RR03RR", "у");
            result = Pattern.Replace("RR04RR", "к");
            result = Pattern.Replace("RR05RR", "е");
            result = Pattern.Replace("RR06RR", "н");
            result = Pattern.Replace("RR07RR", "г");
            result = Pattern.Replace("RR08RR", "ш");
            result = Pattern.Replace("RR09RR", "щ");
            result = Pattern.Replace("RR10RR", "з");
            result = Pattern.Replace("RR11RR", "х");
            result = Pattern.Replace("RR12RR", "ъ");
            result = Pattern.Replace("RR13RR", "ф");
            result = Pattern.Replace("RR14RR", "ы");
            result = Pattern.Replace("RR15RR", "в");
            result = Pattern.Replace("RR16RR", "а");
            result = Pattern.Replace("RR17RR", "п");
            result = Pattern.Replace("RR18RR", "р");
            result = Pattern.Replace("RR19RR", "о");
            result = Pattern.Replace("RR20RR", "л");
            result = Pattern.Replace("RR21RR", "д");
            result = Pattern.Replace("RR22RR", "ж");
            result = Pattern.Replace("RR23RR", "э");
            result = Pattern.Replace("RR24RR", "я");
            result = Pattern.Replace("RR25RR", "ч");
            result = Pattern.Replace("RR26RR", "с");
            result = Pattern.Replace("RR27RR", "м");
            result = Pattern.Replace("RR28RR", "и");
            result = Pattern.Replace("RR29RR", "т");
            result = Pattern.Replace("RR30RR", "ь");
            result = Pattern.Replace("RR31RR", "ё");
            result = Pattern.Replace("RR32RR", "б");
            result = Pattern.Replace("RR33RR", "ю");

            return result;
        }

        public static string CyrToCode(char s)
        {
            switch (s)
            {
                case 'й':
                    return "01";
                case 'ц':
                    return "02";
                case 'у':
                    return "03";
                case 'к':
                    return "04";
                case 'е':
                    return "05";
                case 'н':
                    return "06";
                case 'г':
                    return "07";
                case 'ш':
                    return "08";
                case 'щ':
                    return "09";
                case 'з':
                    return "10";
                case 'х':
                    return "11";
                case 'ъ':
                    return "12";
                case 'ф':
                    return "13";
                case 'ы':
                    return "14";
                case 'в':
                    return "15";
                case 'а':
                    return "16";
                case 'п':
                    return "17";
                case 'р':
                    return "18";
                case 'о':
                    return "19";
                case 'л':
                    return "20";
                case 'д':
                    return "21";
                case 'ж':
                    return "22";
                case 'э':
                    return "23";
                case 'я':
                    return "24";
                case 'ч':
                    return "25";
                case 'с':
                    return "26";
                case 'м':
                    return "27";
                case 'и':
                    return "28";
                case 'т':
                    return "29";
                case 'ь':
                    return "30";
                case 'ё':
                    return "31";
                case 'б':
                    return "32";
                case 'ю':
                    return "33";
                default:
                    return "0";
            }
        }
        public static MvcHtmlString SSI(this HtmlHelper helper, string path, string pathReplace = "")
        {
            using (WebClient client = new WebClient())
            {
                string raw = client.DownloadString(path);
                if (path != "")
                {
                    raw = Regex.Replace(raw, @"(\.\.\/)+", pathReplace);
                    raw = Regex.Replace(raw, @"(\""\/(?<m>[^/]))+", "\"" + pathReplace + "${m}");
                }
                return MvcHtmlString.Create(raw);
            }
        }

    }
}