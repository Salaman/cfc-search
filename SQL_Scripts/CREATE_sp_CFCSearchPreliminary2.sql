USE [CFC2]
GO
/****** Object:  StoredProcedure [dbo].[sp_CFCSearchPreliminary2]    Script Date: 25.02.2021 1:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec [sp_CFCSearchPreliminary] '(FORMSOF (INFLECTIONAL, ленин) OR "ленин") ', 5
*/
--exec sp_CFCSearchPreliminary2 '*', 1, '', '', '', '', '1', '', 0, 0, 0, 0, 0, 0 
--Добавление параметров к основной функции предварительного поиска 10.02.2021 Salaman Andrey
CREATE procedure [dbo].[sp_CFCSearchPreliminary2](@fulltext_pattern nvarchar(500), @searchMode int,
												@subject_name nvarchar(500),
												@archive_institution_name nvarchar(500),
												@archive_full_name nvarchar(500),
												@fund_num_1 varchar(1),
												@fund_num_2 varchar(5),
												@fund_num_3 varchar(1),
												@flag_fund_name int,
												@flag_inventory_name int,
												@flag_annotation_fund_name int,
												@flag_annotation_inventory_name int,
												@flag_history_name int,
												@flag_unit_name int)
as
BEGIN
-- NEW CODE [Salaman Andrey 10/02/2021]
/*
	FundType:
	1 - InFundName
	2 - InFundAnnotate
	3 - InFundHistory
	4 - InInventoryName
	5 - InInventoryAnnotate
	6 - InUnitName
*/

DECLARE @tRes TABLE(
	[Count] INT
)

DECLARE @res INT
--В зависимости от флажков ищем Фонды, Аннотации, Истории и пр. 10.02.2021 Salaman Andrey
	IF(@searchMode = 1 and (@flag_fund_name = 1 or (@flag_fund_name = 0 
													and @flag_inventory_name = 0 
													and @flag_annotation_fund_name = 0 
													and @flag_annotation_inventory_name = 0 
													and @flag_history_name = 0
													and @flag_unit_name = 0 )))
	BEGIN
		--Фонды ищем в зависимости от введенного номера фонда 10.02.2021 Salaman Andrey

			SELECT @res = COUNT(*) FROM dbo.hlpSearch_InFundName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE IF(@searchMode = 2 and (@flag_history_name = 1 or (@flag_fund_name = 0 
															and @flag_inventory_name = 0 
															and @flag_annotation_fund_name = 0 
															and @flag_annotation_inventory_name = 0 
															and @flag_history_name = 0
															and @flag_unit_name = 0)))
	BEGIN

		SELECT @res = COUNT(*)  FROM dbo.hlpSearch_InFundHistory2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE IF(@searchMode = 3 and (@flag_annotation_fund_name = 1 or (@flag_fund_name = 0 
																	and @flag_inventory_name = 0 
																	and @flag_annotation_fund_name = 0 
																	and @flag_annotation_inventory_name = 0 
																	and @flag_history_name = 0
																	and @flag_unit_name = 0)))
	BEGIN

		SELECT @res = COUNT(*)  FROM dbo.hlpSearch_InFundAnnotate2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE IF(@searchMode = 4 and (@flag_inventory_name = 1 or (@flag_fund_name = 0 
															  and @flag_inventory_name = 0 
															  and @flag_annotation_fund_name = 0 
															  and @flag_annotation_inventory_name = 0 
															  and @flag_history_name = 0
															  and @flag_unit_name = 0)))
	BEGIN

		SELECT @res = COUNT_BIG(*)  FROM dbo.hlpSearch_InInventoryName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE IF(@searchMode = 5 and (@flag_annotation_inventory_name = 1 or (@flag_fund_name = 0 
																		 and @flag_inventory_name = 0 
																		 and @flag_annotation_fund_name = 0 
																		 and @flag_annotation_inventory_name = 0 
																		 and @flag_history_name = 0
																		 and @flag_unit_name = 0)))
	BEGIN

		SELECT @res = COUNT_BIG(*)  FROM dbo.hlpSearch_InInventoryAnnotate2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE IF(@searchMode = 6 and (@flag_unit_name = 1 or (@flag_fund_name = 0 
														 and @flag_inventory_name = 0 
													     and @flag_annotation_fund_name = 0 
														 and @flag_annotation_inventory_name = 0 
														 and @flag_history_name = 0
														 and @flag_unit_name = 0)))
	BEGIN

		SELECT @res = COUNT_BIG(*)  FROM dbo.hlpSearch_InUnitName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)

	END
	ELSE 
		SELECT @res = 0 --Если несколько флагов (но не все) = 0 10.02.2021 Salaman Andrey

	SELECT @res AS [Count]
	
END
