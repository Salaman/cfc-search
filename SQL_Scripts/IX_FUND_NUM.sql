alter fulltext index on tblFUND add ([FUND_NUM_1] language [Neutral])
alter fulltext index on tblFUND add ([FUND_NUM_2] language [Neutral])
alter fulltext index on tblFUND add ([FUND_NUM_3] language [Neutral])
go
create fulltext stoplist EmptyStopList;
go
alter fulltext index on tblFUND set stoplist EmptyStopList