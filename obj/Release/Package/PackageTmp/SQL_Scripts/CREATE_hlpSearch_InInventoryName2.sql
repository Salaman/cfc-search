USE [CFC2]
GO
/****** Object:  UserDefinedFunction [dbo].[hlpSearch_InInventoryName2]    Script Date: 19.02.2021 17:17:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Salaman
-- Create date: 11/02/2021
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[hlpSearch_InInventoryName2]
(	
	@fulltext_pattern as nvarchar(500),
	@subject_name as nvarchar(500),
	@archive_institution_name as nvarchar(500),
	@archive_full_name as nvarchar(500),
	@fund_num_1 varchar(1),
	@fund_num_2 varchar(5),
	@fund_num_3 varchar(1)
)
RETURNS @Result TABLE
(
	ID uniqueidentifier,
	Search varchar(max),
	Name varchar(max),
	Code varchar(16),
	SubjectName varchar(300),
	ArchiveNameShort varchar(300),
	ArchiveName varchar(max),
	ArchiveLevel varchar(1),
	FundType int,
	FundName1 varchar(max),
	FundName2 varchar(max),
	FundNum1 varchar(1),
	FundNum2 varchar(5),
	FundNum3 varchar(1)
)
AS
BEGIN
--Если основное поле пустое и номер фонда введен 11.02.2021 Salaman Andrey
IF (@fulltext_pattern = '*' or @fulltext_pattern = '')and @fund_num_2 != ''
		INSERT INTO @Result
							SELECT
								tblINVENTORY.ID as ID,
								tblINVENTORY.INVENTORY_NAME as Search,
								tblINVENTORY.INVENTORY_NAME as Name,
								tblINVENTORY.INVENTORY_NUM_1 + isnull(tblINVENTORY.INVENTORY_NUM_2, '') + isnull(tblINVENTORY.INVENTORY_NUM_3, '') AS Code,
								tblSUBJECT_CL.NAME as SubjectName,
								tblARCHIVE.NAME_SHORT as ArchiveNameShort,
								tblARCHIVE.NAME as ArchiveName,
								tblARCHIVE.ARCHIVE_LEVEL as ArchiveLevel
								, 4 FundType
								, tblARCHIVE.NAME as FundName1
								, tblFUND.FUND_NAME_FULL as FundName2,
								tblFUND.FUND_NUM_1 as FundNum1,
							    tblFUND.FUND_NUM_2 as FundNum2,
							    tblFUND.FUND_NUM_3 as FundNum3
							FROM tblINVENTORY (NOLOCK)
								join tblFUND (NOLOCK) on tblINVENTORY.ISN_FUND = tblFUND.ISN_FUND
								join tblARCHIVE (NOLOCK) on tblFUND.ISN_ARCHIVE = tblARCHIVE.ISN_ARCHIVE
								left join tblSUBJECT_CL (NOLOCK) on tblARCHIVE.ISN_SUBJECT = tblSUBJECT_CL.ISN_SUBJECT
								left join tblSECURLEVEL (NOLOCK) on tblFUND.ISN_SECURLEVEL = tblSECURLEVEL.ISN_SECURLEVEL 
							WHERE tblSECURLEVEL.NAME not in ('с')
								and tblINVENTORY.Deleted = 0 and tblARCHIVE.Deleted = 0 and isnull(tblSUBJECT_CL.Deleted, 0) = 0 and isnull(tblSECURLEVEL.Deleted, 0) = 0
								and ((tblFUND.FUND_NUM_1 like @fund_num_1 or tblFUND.FUND_NUM_1 is null) --Поиск только по номеру фонда 11.02.2021 Salaman Andrey
								and contains(tblFUND.FUND_NUM_2, @fund_num_2)
								and (tblFUND.FUND_NUM_3 like @fund_num_3 or tblFUND.FUND_NUM_3 is null))

							and CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(tblSUBJECT_CL.NAME))) ELSE '' END = 
							CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(@subject_name))) ELSE '' END
							and 
							( CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME))) ELSE '' END = 
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							or
							CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME_SHORT))) ELSE '' END =
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							)

--Если основное поле введено и номер фонда введен 11.02.2021 Salaman Andrey
ELSE IF (@fulltext_pattern != '*' or @fulltext_pattern != '') and @fund_num_2 != ''
		INSERT INTO @Result
				SELECT * FROM (
							SELECT
								tblINVENTORY.ID as ID,
								tblINVENTORY.INVENTORY_NAME as Search,
								tblINVENTORY.INVENTORY_NAME as Name,
								tblINVENTORY.INVENTORY_NUM_1 + isnull(tblINVENTORY.INVENTORY_NUM_2, '') + isnull(tblINVENTORY.INVENTORY_NUM_3, '') AS Code,
								tblSUBJECT_CL.NAME as SubjectName,
								tblARCHIVE.NAME_SHORT as ArchiveNameShort,
								tblARCHIVE.NAME as ArchiveName,
								tblARCHIVE.ARCHIVE_LEVEL as ArchiveLevel
								, 4 FundType
								, tblARCHIVE.NAME as FundName1
								, tblFUND.FUND_NAME_FULL as FundName2,
								tblFUND.FUND_NUM_1 as FundNum1,
							    tblFUND.FUND_NUM_2 as FundNum2,
							    tblFUND.FUND_NUM_3 as FundNum3
							FROM tblINVENTORY (NOLOCK)
								join tblFUND (NOLOCK) on tblINVENTORY.ISN_FUND = tblFUND.ISN_FUND
								join tblARCHIVE (NOLOCK) on tblFUND.ISN_ARCHIVE = tblARCHIVE.ISN_ARCHIVE
								left join tblSUBJECT_CL (NOLOCK) on tblARCHIVE.ISN_SUBJECT = tblSUBJECT_CL.ISN_SUBJECT
								left join tblSECURLEVEL (NOLOCK) on tblFUND.ISN_SECURLEVEL = tblSECURLEVEL.ISN_SECURLEVEL
							WHERE tblSECURLEVEL.NAME not in ('с')
								and tblINVENTORY.Deleted = 0 and tblARCHIVE.Deleted = 0 and isnull(tblSUBJECT_CL.Deleted, 0) = 0 and isnull(tblSECURLEVEL.Deleted, 0) = 0
								and (contains(tblINVENTORY.INVENTORY_NAME, @fulltext_pattern, language 'Russian'))) as NameSearch
						WHERE (FundNum1 like @fund_num_1 or FundNum1 is null)--Ограничение поиска по основному полю 11.02.2021 Salaman Andrey
						and FundNum2 like @fund_num_2
						and (FundNum3 like @fund_num_3 or FundNum3 is null)

						and CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(SubjectName))) ELSE '' END = 
							CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(@subject_name))) ELSE '' END
							and 
							( CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(ArchiveName))) ELSE '' END = 
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							or
							CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(ArchiveNameShort))) ELSE '' END =
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							)

ELSE IF (@fulltext_pattern = '*' or @fulltext_pattern = '') and @fund_num_2 = ''

		INSERT INTO @Result
							SELECT
								tblINVENTORY.ID as ID,
								tblINVENTORY.INVENTORY_NAME as Search,
								tblINVENTORY.INVENTORY_NAME as Name,
								tblINVENTORY.INVENTORY_NUM_1 + isnull(tblINVENTORY.INVENTORY_NUM_2, '') + isnull(tblINVENTORY.INVENTORY_NUM_3, '') AS Code,
								tblSUBJECT_CL.NAME as SubjectName,
								tblARCHIVE.NAME_SHORT as ArchiveNameShort,
								tblARCHIVE.NAME as ArchiveName,
								tblARCHIVE.ARCHIVE_LEVEL as ArchiveLevel
								, 4 FundType
								, tblARCHIVE.NAME as FundName1
								, tblFUND.FUND_NAME_FULL as FundName2,
								tblFUND.FUND_NUM_1 as FundNum1,
							    tblFUND.FUND_NUM_2 as FundNum2,
							    tblFUND.FUND_NUM_3 as FundNum3
							FROM tblINVENTORY (NOLOCK)
								join tblFUND (NOLOCK) on tblINVENTORY.ISN_FUND = tblFUND.ISN_FUND
								join tblARCHIVE (NOLOCK) on tblFUND.ISN_ARCHIVE = tblARCHIVE.ISN_ARCHIVE
								left join tblSUBJECT_CL (NOLOCK) on tblARCHIVE.ISN_SUBJECT = tblSUBJECT_CL.ISN_SUBJECT
								left join tblSECURLEVEL (NOLOCK) on tblFUND.ISN_SECURLEVEL = tblSECURLEVEL.ISN_SECURLEVEL
							WHERE tblSECURLEVEL.NAME not in ('с')
								and tblINVENTORY.Deleted = 0 and tblARCHIVE.Deleted = 0 and isnull(tblSUBJECT_CL.Deleted, 0) = 0 and isnull(tblSECURLEVEL.Deleted, 0) = 0
								--and (contains(tblINVENTORY.INVENTORY_NAME, @fulltext_pattern, language 'Russian'))
								--Обычный поиск по основному полю 11.02.2021 Salaman Andrey
							and CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(tblSUBJECT_CL.NAME))) ELSE '' END = 
							CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(@subject_name))) ELSE '' END
							and 
							( CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME))) ELSE '' END = 
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							or
							CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME_SHORT))) ELSE '' END =
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							)

--В других случаях 11.02.2021 Salaman Andrey
ELSE
		INSERT INTO @Result
							SELECT
								tblINVENTORY.ID as ID,
								tblINVENTORY.INVENTORY_NAME as Search,
								tblINVENTORY.INVENTORY_NAME as Name,
								tblINVENTORY.INVENTORY_NUM_1 + isnull(tblINVENTORY.INVENTORY_NUM_2, '') + isnull(tblINVENTORY.INVENTORY_NUM_3, '') AS Code,
								tblSUBJECT_CL.NAME as SubjectName,
								tblARCHIVE.NAME_SHORT as ArchiveNameShort,
								tblARCHIVE.NAME as ArchiveName,
								tblARCHIVE.ARCHIVE_LEVEL as ArchiveLevel
								, 4 FundType
								, tblARCHIVE.NAME as FundName1
								, tblFUND.FUND_NAME_FULL as FundName2,
								tblFUND.FUND_NUM_1 as FundNum1,
							    tblFUND.FUND_NUM_2 as FundNum2,
							    tblFUND.FUND_NUM_3 as FundNum3
							FROM tblINVENTORY (NOLOCK)
								join tblFUND (NOLOCK) on tblINVENTORY.ISN_FUND = tblFUND.ISN_FUND
								join tblARCHIVE (NOLOCK) on tblFUND.ISN_ARCHIVE = tblARCHIVE.ISN_ARCHIVE
								left join tblSUBJECT_CL (NOLOCK) on tblARCHIVE.ISN_SUBJECT = tblSUBJECT_CL.ISN_SUBJECT
								left join tblSECURLEVEL (NOLOCK) on tblFUND.ISN_SECURLEVEL = tblSECURLEVEL.ISN_SECURLEVEL
							WHERE tblSECURLEVEL.NAME not in ('с')
								and tblINVENTORY.Deleted = 0 and tblARCHIVE.Deleted = 0 and isnull(tblSUBJECT_CL.Deleted, 0) = 0 and isnull(tblSECURLEVEL.Deleted, 0) = 0
								and (contains(tblINVENTORY.INVENTORY_NAME, @fulltext_pattern, language 'Russian'))
								--Обычный поиск по основному полю 11.02.2021 Salaman Andrey
							and CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(tblSUBJECT_CL.NAME))) ELSE '' END = 
							CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(@subject_name))) ELSE '' END
							and 
							( CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME))) ELSE '' END = 
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							or
							CASE WHEN @archive_institution_name != '' OR @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(tblARCHIVE.NAME_SHORT))) ELSE '' END =
							CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE '' END
							)

RETURN
END
