USE [CFC2]
GO
/****** Object:  StoredProcedure [dbo].[sp_CFCSearch2]    Script Date: 19.02.2021 17:15:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Добавлены новые параметры к функции поиска при нажатии "Показать все" 10.02.2021 Salaman Andrey
CREATE procedure [dbo].[sp_CFCSearch2](@fulltext_pattern nvarchar(500), @match_pattern nvarchar(500), @filter varchar(500), @top_qty int, @end_qty int,
									   @subject_name nvarchar(500),
									   @archive_institution_name nvarchar(500),
									   @archive_full_name nvarchar(500),
									   @fund_num_1 varchar(1),
									   @fund_num_2 varchar(5),
									   @fund_num_3 varchar(1),
									   @flag_fund_name int,
									   @flag_inventory_name int,
									   @flag_annotation_fund_name int,
									   @flag_annotation_inventory_name int,
									   @flag_history_name int,
									   @flag_unit_name int)
as
BEGIN

	declare @time1 as datetime
	set @time1=GETDATE()

BEGIN -- NEW CODE [Salaman Andrey 10/02/2021]

	
create table #firstLevelData  (
    [row_num][int] ,
    [ID] [uniqueidentifier],
	[ID_Search] [uniqueidentifier],
	[ID_Doc] [uniqueidentifier],
	[Search] [nvarchar](MAX),
	[Name] [nvarchar](MAX),
	[Code] [nvarchar](MAX),
	[SubjectName] [nvarchar](MAX),
	[ArchiveName] [nvarchar](MAX),
	[ArchiveNameShort] [nvarchar](MAX),
	[ArchiveLevel] [nvarchar](MAX),
	[Filter] [nvarchar](MAX),
	[Kind] [int] ,
	[Header] [nvarchar](MAX),
	[Map] [nvarchar](MAX)
	, FundType int
	, FundName1 nvarchar(max)	
	, FundName2 nvarchar(max)
	, FundName3 nvarchar(max)
)

/*
	FundType:
	1 - InFundName
	2 - InFundAnnotate
	3 - InFundHistory
	4 - InInventoryName
	5 - InInventoryAnnotate
	6 - InUnitName
*/


--SET rowcount   @top_qty  
	IF len(@filter)=0
	BEGIN
		SET @filter='InFundName;'
	END

	IF (isnull(@filter, '') = '' or patindex('%InFundName%', @filter) > 0)
	BEGIN
	
		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1)

		SELECT		
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
			FROM hlpSearch_InFundName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)--Фонды ищем в зависимости от введенного номера фонда 10.02.2021 Salaman Andrey
		) as x WHERE row_num < @end_qty 

	END
					
	IF (patindex('%InFundAnnotate%', @filter) > 0)
	BEGIN

		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1)
		 				

		SELECT		
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
			FROM hlpSearch_InFundAnnotate2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1, @fund_num_2, @fund_num_3)
		) as x WHERE row_num < @end_qty 
	END

	IF (patindex('%InFundHistory%', @filter) > 0)
	BEGIN

		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1)
		SELECT
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
			FROM hlpSearch_InFundHistory2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)
		) as x WHERE row_num < @end_qty 
	END

	IF (patindex('%InInventoryName%', @filter) > 0)
	BEGIN

		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1, FundName2)

		SELECT		
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
			, FundName2
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
				, FundName2
			FROM hlpSearch_InInventoryName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)--Опись ищем в зависимости от введенного номера фонда 11.02.2021 Salaman Andrey
		) as x WHERE row_num < @end_qty 
	END
	
	IF (patindex('%InInventoryAnnotate%', @filter) > 0)
	BEGIN

		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1, FundName2)

		SELECT		
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
			, FundName2
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,				
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
				, FundName2
			FROM hlpSearch_InInventoryAnnotate2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)
		) as x WHERE row_num < @end_qty 
	END 

	IF (patindex('%InUnitName%', @filter) > 0)
	BEGIN

		INSERT INTO #firstLevelData(ID, Search, Name, Code, SubjectName, ArchiveName, ArchiveNameShort, ArchiveLevel
			, FundType, FundName1, FundName2, FundName3)

		SELECT		
			ID
			, Search
			, [Name]
			, Code
			, SubjectName
			, ArchiveNameShort
			, ArchiveName
			, ArchiveLevel
			, FundType
			, FundName1
			, FundName2
			, FundName3
		 FROM (
			SELECT
				row_number() over(ORDER BY id) row_num,
				ID
				, Search
				, [Name]
				, Code
				, SubjectName
				, ArchiveNameShort
				, ArchiveName
				, ArchiveLevel
				, FundType
				, FundName1
				, FundName2
				, FundName3
			FROM hlpSearch_InUnitName2(@fulltext_pattern, @subject_name, @archive_institution_name, @archive_full_name, @fund_num_1,@fund_num_2,@fund_num_3)
		) as x WHERE row_num < @end_qty 
	END

	-- Добавлена фильтрация по субъекту, названию архивного учреждения и полному названию архива 19.02.2021 Utkin Dmitry
/*	select *
	into #secondLevelData 
	FROM 
	(
		SELECT
			distinct row_number() over(ORDER BY id) rNum, * 
		from #firstLevelData
		WHERE 
			UPPER(LTRIM(RTRIM(SubjectName))) = 
			CASE WHEN @subject_name != '' THEN UPPER(LTRIM(RTRIM(@subject_name))) ELSE UPPER(LTRIM(RTRIM(SubjectName))) END
			and 
			( UPPER(LTRIM(RTRIM(ArchiveName))) = 
			CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE UPPER(LTRIM(RTRIM(ArchiveName))) END
			or
			UPPER(LTRIM(RTRIM(ArchiveNameShort))) = 
			CASE WHEN @archive_institution_name != '' THEN UPPER(LTRIM(RTRIM(@archive_institution_name))) WHEN @archive_full_name != '' THEN UPPER(LTRIM(RTRIM(@archive_full_name))) ELSE UPPER(LTRIM(RTRIM(ArchiveNameShort))) END
			)
	) as x
	WHERE
		rNum BETWEEN @top_qty AND @end_qty 
*/

	select *
	into #secondLevelData 
	FROM 
	(
		SELECT
			distinct row_number() over(ORDER BY id) rNum, * 
		from #firstLevelData
	) as x
	WHERE 
		rNum BETWEEN @top_qty AND @end_qty 


/*		
		select *
	into #secondLevelData 
	from #firstLevelData 
	*/

	UPDATE #secondLevelData
	SET
		row_num = rNum
		, Search = substring(ISNULL(Search,'-') ,1,999)
		, [Name] = substring(ISNULL([Name],'-'),1,999)
		, SubjectName = isnull(SubjectName,'-')
		, ArchiveName = isnull(ArchiveName,'-')
		, ArchiveNameShort = isnull(ArchiveNameShort,'-')
		, ArchiveLevel = isnull(ArchiveLevel,'-')
		, [Filter] = 
			CASE FundType
				WHEN 1 THEN N'InFundName'
				WHEN 2 THEN N'InFundAnnotate'
				WHEN 3 THEN N'InFundHistory'
				WHEN 4 THEN N'InInventoryName'
				WHEN 5 THEN N'InInventoryAnnotate'
				WHEN 6 THEN N'InUnitName'
			END
		, [Kind] = 
			CASE FundType
				WHEN 1 THEN 701
				WHEN 2 THEN 701
				WHEN 3 THEN 701
				WHEN 4 THEN 702
				WHEN 5 THEN 702
				WHEN 6 THEN 703
			END
		, [Header] = 
			CASE FundType
				WHEN 1 THEN N'Фонд'
				WHEN 2 THEN N'Фонд'
				WHEN 3 THEN N'Фонд'
				WHEN 4 THEN N'Опись'
				WHEN 5 THEN N'Опись'
				WHEN 6 THEN N'Единица хранения'
			END
		, Map = COALESCE('Архив: ' + FundName1, '') + COALESCE(' >> Фонд: ' + FundName2, '') + COALESCE(' >> Опись: ' + FundName3, '')

END

	SELECT 
		row_num,
		ID,
		substring(Search,1,999) as Search,
		substring(Name,1,999) as Name,
		Code,
		substring(SubjectName,1,999) as SubjectName,
		substring(ArchiveName,1,999) as ArchiveName,
		substring(ArchiveNameShort,1,999) as ArchiveNameShort,
		ArchiveLevel,
		Filter,
		Kind,
		FundType,
		Header,
		Map 
	FROM #secondLevelData
	ORDER BY row_num

	DROP TABLE #secondLevelData
	DROP TABLE #firstLevelData
 /*                   
	 INSERT INTO [dbo].[tblCFCSearchStatistic]
			   (ID
			   ,[Search_1]
			   ,[Search_2]
			   ,[CreationDateTime]
			   ,[EndDateTime]
			   ,[ExecTime])
		 VALUES
			  (NEWID()
			  ,@match_pattern
			  ,@fulltext_pattern
			   ,@time1
			   ,GETDATE()
			   ,DATEDIFF(ms,@time1,GETDATE()))
*/

--declare @firstLevelData  table(
--	row_num int,
--	ID uniqueidentifier,
--	Search nvarchar(1000),
--	Name nvarchar(1000),
--	Code nvarchar(255),
--	SubjectName nvarchar(1000),
--	ArchiveName nvarchar(1000),
--	ArchiveNameShort nvarchar(1000),
--	ArchiveLevel nvarchar,
--	Filter nvarchar(255),
--	Kind int,
--	FundType int,
--	Header nvarchar(255),
--	Map nvarchar(255)
--)

--select * from @firstLevelData



END
